## YouTube Mini Player

This is a fork of [Better YouTube](https://greasyfork.org/en/scripts/12250-better-youtube) by Feifei Hang, v5.1, aka YouTube Mini Player.

The downloader, space to play/pause, and side-to-side features of Better YouTube are unnecessary bloat, and it seems Hang has ceased maintainence of this script. So, I've forked the useful bits and plan to refactor and fine tune if necessary. 

Some immediate things that jump out to me (consider this the TODO): 

* the on/off switch is not visible in the new YouTube theme (it's probably hidden under the header)
* the video doesn't fill the mini player box
* some of the alignment in the youtube player itself is awkward (might not be a trivial fix)

Outside help is welcome. You know the drill. Make a fork, implement your fixes, and submit a pull request.

## Legal/license info

YouTube Mini Player/Better YouTube is unlicensed and thus under public domain. So to respect that, this fork shall remain under public domain as well.
